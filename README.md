# parametrictype.bitbucket.io

Website for student work.

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---

## Introduction

Parametric is a website that presents student work for a workshop given at Cambrai ESAC, France. The workshop explored parametric type design using custom-made and open source tools.

## Contents

* [Source: website]()
*

## Website

The repository is a website : [Parametric](https://parametrictype.bitbucket.io/)

## Install

The files in this repository can be compiled in any internet browser.

## Contact & Sundries

* mw@fabprojects.codes
* Version v0.1
* Tools used : alphaGraph

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
